import React, { useState } from 'react';

// Components
import { ThemeProvider } from '@material-ui/core';
import Navbar from './components/Navbar';

import './App.scss';

// Theme
import Theme from './config/theme';

// Routes
import Routes from './config/routes';

function App() {
    const [searchKeyword, setSearchKeyword] = useState('');

    const onSearch = value => {
        setSearchKeyword(value);
    };

    return (
        <ThemeProvider theme={Theme}>
            <div className="App">
                <Navbar onSearch={onSearch} />
                <div className="App__content">
                    <Routes searchKeyword={searchKeyword} />
                </div>
            </div>
        </ThemeProvider>
    );
}

export default App;
