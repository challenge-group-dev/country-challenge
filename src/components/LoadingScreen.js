import React from 'react';

// Components
import { CircularProgress } from '@material-ui/core';

// Styles
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
        width: '100vw',
        height: '100vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        background: 'rgba(255,255,255,0.1)',
    },
});

function LoadingScreen() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <CircularProgress color="secondary" /> &nbsp; Loading...
        </div>
    );
}

export default LoadingScreen;
