import React from 'react';

// Components
import {
    AppBar,
    Toolbar,
    IconButton,
    Typography,
    InputBase,
} from '@material-ui/core';

// Styles
import { fade, makeStyles } from '@material-ui/core/styles';

// Icons
import MapIcon from '@material-ui/icons/Map';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles(theme => ({
    navbar: {
        padding: '0.3rem 1rem',
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
}));

const Navbar = ({ onSearch }) => {
    const classes = useStyles();

    const onSearchHandler = e => {
        const { value } = e.target;
        if (value.length > 0 && value.length < 3) return;
        onSearch(value);
    };

    return (
        <AppBar position="fixed" color="primary">
            <Toolbar className={classes.navbar} variant="dense">
                <IconButton edge="start" color="inherit" aria-label="menu">
                    <MapIcon />
                </IconButton>
                <Typography variant="h6" color="inherit">
                    Countries Challenge
                </Typography>
                <div className={classes.search}>
                    <div className={classes.searchIcon}>
                        <SearchIcon />
                    </div>
                    <InputBase
                        placeholder="Search for a country…"
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                        data-testid="searchbar"
                        inputProps={{
                            'aria-label': 'search',
                            'data-testid': 'search-input',
                        }}
                        onChange={value => onSearchHandler(value)}
                    />
                </div>
            </Toolbar>
        </AppBar>
    );
};

export default Navbar;
