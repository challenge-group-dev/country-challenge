import React from 'react';

// Components
import { Drawer, DialogTitle, IconButton } from '@material-ui/core';

// Icons
import { Close as CloseIcon } from '@material-ui/icons';

// Styles
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    drawer: {
        maxWidth: '30%',
        width: '30%',
        padding: '1rem',
        boxSizing: 'border-box',
    },
    drawerTitle: {
        display: 'flex',
        justifyContent: 'flex-end',
        padding: '0',
    },
    closeIcon: {
        float: 'right',
    },
}));

function SideDrawer({ open, onClose, children }) {
    const classes = useStyles();

    return (
        <Drawer
            anchor="right"
            open={open}
            onClose={onClose}
            PaperProps={{
                className: classes.drawer,
                'data-testid': 'side-drawer',
            }}
        >
            <DialogTitle className={classes.drawerTitle}>
                <IconButton className={classes.closeIcon} onClick={onClose}>
                    <CloseIcon className={classes.close} />
                </IconButton>
            </DialogTitle>
            {children}
        </Drawer>
    );
}

export default SideDrawer;
