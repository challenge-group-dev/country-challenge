import React from 'react';

// Components
import {
    Card,
    CardMedia,
    CardContent,
    CardActionArea,
    Typography,
} from '@material-ui/core';

// Styles
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        margin: '1rem',
    },
    details: {
        display: 'flex',
        flex: '1 0 auto',
        flexDirection: 'column',
        alignSelf: 'flex-start',
    },
    content: {},
    cover: {
        width: 151,
    },
    name: {
        wordWrap: 'break-word',
        fontSize: '16px',
    },
}));

function CountryCard({ id, name, capital, flag, className, onClick }) {
    const classes = useStyles();
    return (
        <Card className={classes.root}>
            <div data-testid="country-card" className={classes.details}>
                <CardActionArea
                    data-testid="country-card-action"
                    onClick={onClick}
                >
                    <CardContent className={classes.content}>
                        <Typography
                            className={classes.name}
                            component="h6"
                            variant="h6"
                        >
                            {name}
                        </Typography>
                        <Typography variant="subtitle1" color="textSecondary">
                            {capital}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </div>
            <CardMedia
                className={classes.cover}
                image={flag}
                title="Live from space album cover"
            />
        </Card>
    );
}

export default CountryCard;
