import React from 'react';
import { render } from '@testing-library/react';
import { CountryCard } from 'components';

describe('Country Card testing', () => {
    const props = {
        id: '1',
        name: 'Teste',
        capital: 'Capital',
        flag: 'https://restcountries.eu/data/dza.svg',
        onClick: jest.fn(),
    };

    it('Should render country card', () => {
        const component = renderComponent(props);
        expect(component).toBeDefined();
    });
});

const renderComponent = props => render(<CountryCard {...props} />);
