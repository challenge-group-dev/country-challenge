import React from 'react';
import { render, fireEvent, act } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import { Navbar } from 'components';

describe('Navbar testing', () => {
    const props = {
        onSearch: jest.fn(),
    };

    let wrapper = null;
    beforeEach(() => {
        wrapper = render(
            <MockedProvider mocks={[]}>
                <Navbar {...props} />
            </MockedProvider>
        );
    });

    it('Should render navbar search', () => {
        const navbar = wrapper.getByTestId('searchbar');
        expect(navbar).toBeInTheDocument();
    });

    it('Should call onSearch', () => {
        const navbar = wrapper.getByTestId('search-input');

        act(() => {
            fireEvent.change(navbar, { target: { value: 'italo maio' } });
            fireEvent.change(navbar, { target: { value: 'it' } });
        });

        expect(props.onSearch).toHaveBeenCalled();
    });
});
