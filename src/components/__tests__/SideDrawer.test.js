import React from 'react';
import { render } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import { SideDrawer } from 'components';

describe('SideDrawer testing', () => {
    const mocks = {
        onClose: jest.fn(),
    };

    let wrapper = null;
    beforeEach(() => {
        wrapper = render(
            <MockedProvider mocks={[]}>
                <SideDrawer open onClose={mocks.onClose} />
            </MockedProvider>
        );
    });

    it('Should render side drawer', () => {
        const drawer = wrapper.getByTestId('side-drawer');
        expect(drawer).toBeInTheDocument();
    });
});
