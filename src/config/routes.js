import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// Components
import Home from '../pages/Home';

export default ({ searchKeyword }) => (
    <Router>
        <Switch>
            <Route
                path="/"
                render={({ onClick }) => (
                    <Home searchKeyword={searchKeyword} onClick={onClick} />
                )}
            />
        </Switch>
    </Router>
);
