import { createMuiTheme } from '@material-ui/core';

import { purple, green } from '@material-ui/core/colors';

export default createMuiTheme({
    pallete: {
        primary: {
            main: purple[500],
        },
        secondary: {
            main: green[500],
        },
    },
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
});
