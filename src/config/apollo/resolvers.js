import { gql } from '@apollo/client';

export const resolvers = {
    Query: {
        Global: (_, variables, { cache, getCacheKey }) => {
            const id = getCacheKey({ __typename: `Global`, id: 'Soft' });
            const fragment = gql`
                fragment updateSearchKeyWord on Global {
                    searchKeyword
                }
            `;

            const search = cache.readFragment({ fragment, id });

            return search;
        },
    },
    Mutation: {
        updateCountry: (_, variables, { cache, getCacheKey }) => {
            const id = getCacheKey({ __typename: `Country`, id: variables.id });

            const fragment = gql`
                fragment updateCountry on Country {
                    name
                    area
                    population
                }
            `;

            const country = cache.readFragment({ fragment, id });

            cache.writeData({
                id,
                data: {
                    ...country,
                    area: variables.data.area,
                    population: variables.data.population,
                },
            });

            return country;
        },
        updateSearchKeyword: (_, variables, { cache, getCacheKey }) => {
            const id = getCacheKey({ __typename: `Global`, id: 'Soft' });

            const fragment = gql`
                fragment updateSearchKeyWord on Global {
                    searchKeyword
                }
            `;

            const search = cache.readFragment({ fragment, id });

            cache.writeData({
                id,
                data: {
                    searchKeyword: variables.input,
                },
                ...search,
            });
        },
    },
};

export const typeDefs = gql`
    extend type Mutation {
        updateCountry(id: ID!): [ID!]!
        updateSearchKeyword(input: String!): [String!]!
    }
`;
