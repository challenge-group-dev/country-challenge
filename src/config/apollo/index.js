import { ApolloClient, HttpLink } from 'apollo-boost';
import { resolvers, typeDefs } from './resolvers';
import { cache } from './cache';

cache.writeData({
    id: 'Global:Soft',
    data: {
        searchKeyword: '',
    },
});

export default new ApolloClient({
    link: new HttpLink({ uri: 'https://countries-274616.ew.r.appspot.com/' }),
    resolvers,
    typeDefs,
    connectToDevTools: true,
    cache,
});
