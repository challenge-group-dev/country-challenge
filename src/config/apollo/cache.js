import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-boost';

export const cache = new InMemoryCache({
    fragmentMatcher: new IntrospectionFragmentMatcher({
        introspectionQueryResultData: {
            __schema: {
                types: [],
            },
        },
    }),
    typePolicies: {
        Country: {
            fields: {
                ItaloMaio: {
                    read() {
                        return 'ASJDAUSUAHS';
                    },
                },
            },
        },
    },
});
