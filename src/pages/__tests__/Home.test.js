import React from 'react';
import { render } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { MockedProvider } from '@apollo/client/testing';
import { Router } from 'react-router-dom';

import { GET_COUNTRIES_QUERY } from 'services/country/useGetCountries';

import Home from '../Home';

describe('Home testing', () => {
    const props = {
        searchKeyword: '',
    };

    const gqlMock = [
        {
            request: {
                query: GET_COUNTRIES_QUERY,
                variables: {
                    filter: {},
                    first: 20,
                    offset: 0,
                },
            },
            result: {
                data: {
                    Country: [
                        {
                            _id: '1',
                            name: 'Marcelino',
                            capital: 'teste',
                            flag: { svgFile: 'teste', __typename: 'Flag' },
                            __typename: 'Country',
                        },
                        {
                            _id: '2',
                            name: 'Marcelino',
                            capital: 'teste',
                            flag: { svgFile: 'teste', __typename: 'Flag' },
                            __typename: 'Country',
                        },
                    ],
                },
            },
        },
        {
            request: {
                query: GET_COUNTRIES_QUERY,
                variables: {
                    filter: { name_contains: props.searchKeyword },
                    first: 20,
                    offset: 0,
                },
            },
            result: {
                data: {
                    Country: [
                        {
                            _id: '1',
                            name: 'Marcelino',
                            capital: 'teste',
                            flag: { svgFile: 'teste', __typename: 'Flag' },
                            __typename: 'Country',
                        },
                        {
                            _id: '2',
                            name: 'Marcelino',
                            capital: 'teste',
                            flag: { svgFile: 'teste', __typename: 'Flag' },
                            __typename: 'Country',
                        },
                    ],
                },
            },
        },
    ];

    let wrapper = null;
    const history = createMemoryHistory();

    beforeAll(() => {
        wrapper = render(
            <MockedProvider mocks={gqlMock}>
                <Router history={history}>
                    <Home {...props} />
                </Router>
            </MockedProvider>
        );
    });

    it('Should render correctly', () => {
        expect(wrapper).toBeDefined();
    });
});
