import React, { useEffect, useCallback } from 'react';

// Router
import { useHistory, Route, Switch } from 'react-router-dom';

// Components
import CountryCard from '../components/CountryCard';
import { LoadingScreen } from 'components';
import { Fab } from '@material-ui/core';

// Icons
import { Autorenew as AutorenewIcon } from '@material-ui/icons';

// Page
import Details from './Details';

// Styles
import { makeStyles } from '@material-ui/core/styles';

// Utils
import { isNullOrUndefined } from '../utils/general';

// Services
import useGetCountries from '../services/country/useGetCountries';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-evenly',
    },
    card: {
        minWidth: '30%',
    },
    fab: {
        position: 'fixed',
        bottom: '2rem',
    },
}));

function Home({ searchKeyword }) {
    const classes = useStyles();
    const history = useHistory();

    const { countries, refetch, loading, fetchMore } = useGetCountries({
        first: 20,
        offset: 0,
    });

    const fetchMoreCountries = useCallback(() => {
        fetchMore({
            variables: { offset: countries.length },
            updateQuery: (prev, { fetchMoreResult }) => {
                if (!fetchMoreResult) return prev;
                return Object.assign({}, prev, {
                    Country: [...prev.Country, ...fetchMoreResult.Country],
                });
            },
        });
    }, [countries.length, fetchMore]);

    const goToCountry = id => history.push(`/${id}`);

    useEffect(() => {
        refetch({
            filter: {
                name_contains: searchKeyword,
            },
        });
    }, [searchKeyword, refetch]);

    return (
        <div data-testid="home-screen" className={classes.root}>
            {loading ? (
                <LoadingScreen />
            ) : (
                countries
                    ?.filter(c => !isNullOrUndefined(c.capital))
                    .map(item => (
                        <div className={classes.card} key={item._id}>
                            <CountryCard
                                onClick={() => goToCountry(item._id)}
                                className={classes.card}
                                id={item._id}
                                name={item.name}
                                capital={item.capital}
                                flag={item.flag.svgFile}
                            />
                        </div>
                    ))
            )}
            <div className={classes.fab}>
                <Fab
                    onClick={() => fetchMoreCountries()}
                    variant="extended"
                    color="secondary"
                    data-testid="fab-load-more"
                >
                    <AutorenewIcon /> &nbsp; Load more
                </Fab>
            </div>
            <Switch>
                <Route path="/:id" component={Details} />
            </Switch>
        </div>
    );
}

export default Home;
