import React, { useState, useEffect } from 'react';

// Router
import { useParams, useHistory } from 'react-router-dom';

// Components
import { SideDrawer } from 'components';
import {
    Grid,
    Button,
    FormControlLabel,
    Checkbox,
    Paper,
} from '@material-ui/core';

// Icons
import { Save as SaveIcon } from '@material-ui/icons';

// Internal Components
import FlagCardDetails from './components/FlagCardDetails';
import MapCard from './components/MapCard';
import InputArea from './components/InputArea';
import InputPopulation from './components/InputPopulation';
import DomainsChips from './components/DomainsChips';

// Services
import useGetCountry from 'services/country/useGetCountry';
import useUpdateCountry from 'services/country/useUpdateCountry';

function Details() {
    const [drawerOpened, setDrawerOpened] = useState(false);
    const [editMode, setEditMode] = useState(false);
    const [countryModel, setCountryModel] = useState({});

    const history = useHistory();
    const { id } = useParams();

    const { country, loading } = useGetCountry({
        id: id,
    });

    const { onUpdateCountry } = useUpdateCountry();

    const handleClose = () => {
        setDrawerOpened(false);
        setTimeout(() => {
            history.goBack();
        }, 200);
    };

    const handleAreaChange = e => {
        if (!editMode) return;
        const { value } = e.target;
        setCountryModel(old => ({
            ...old,
            area: value,
        }));
    };

    const handlePopulationChange = e => {
        if (!editMode) return;
        const { value } = e.target;
        setCountryModel(old => ({
            ...old,
            population: value,
        }));
    };

    const handleSave = () => {
        if (!editMode) return;
        console.log(countryModel);
        onUpdateCountry({
            variables: {
                id: countryModel._id,
                data: {
                    ...countryModel,
                },
            },
        });
        setEditMode(false);
    };

    useEffect(() => {
        setDrawerOpened(true);
    }, []);

    useEffect(() => {
        setCountryModel(country);
    }, [country]);

    return (
        <SideDrawer open={drawerOpened} onClose={handleClose}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <FlagCardDetails loading={loading} country={countryModel} />
                </Grid>
                <Grid item xs={4}>
                    <InputArea
                        onChange={e => handleAreaChange(e)}
                        value={countryModel?.area}
                        loading={loading}
                        disabled={!editMode}
                    />
                </Grid>
                <Grid item xs={4}>
                    <InputPopulation
                        onChange={e => handlePopulationChange(e)}
                        value={countryModel?.population}
                        loading={loading}
                        disabled={!editMode}
                    />
                </Grid>
                <Grid item xs={4}>
                    <DomainsChips
                        disabled={!editMode}
                        domains={countryModel?.topLevelDomains}
                        loading={loading}
                    />
                </Grid>
                <Grid item xs={12}>
                    <MapCard
                        center={{
                            latitude: countryModel?.location?.latitude,
                            longitude: countryModel?.location?.longitude,
                        }}
                        countryId={countryModel?._id}
                    />
                </Grid>
                <div
                    style={{
                        position: 'fixed',
                        bottom: 20,
                        right: 20,
                        display: 'flex',
                    }}
                >
                    <Paper
                        style={{ padding: '0 0.5rem', margin: '0 0.5rem 0 0' }}
                    >
                        <FormControlLabel
                            style={{ margin: '0 0.7rem 0 0' }}
                            control={
                                <Checkbox
                                    checked={!!editMode}
                                    onChange={e =>
                                        setEditMode(e.target.checked)
                                    }
                                    color="secondary"
                                />
                            }
                            label="Edition mode"
                        />
                    </Paper>
                    <Button
                        variant="contained"
                        color="secondary"
                        size="large"
                        startIcon={<SaveIcon />}
                        onClick={() => handleSave()}
                    >
                        Save
                    </Button>
                </div>
            </Grid>
        </SideDrawer>
    );
}

export default Details;
