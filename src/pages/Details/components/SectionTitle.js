import React from 'react';

// Components
import { Typography } from '@material-ui/core';

// Styles
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    sectionTitle: {
        display: 'flex',
        alignItems: 'center',
        color: 'grey',
        marginBottom: '0.7rem',
    },
    sectionIcon: {
        marginRight: '0.5rem',
    },
});

function SectionTitle({ icon: Icon, title }) {
    const classes = useStyles();
    return (
        <Typography
            className={classes.sectionTitle}
            component="h5"
            variant="h5"
        >
            <Icon className={classes.sectionIcon} />
            {title}
        </Typography>
    );
}

export default SectionTitle;
