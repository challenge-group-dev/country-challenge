import React from 'react';

// Components
import { Grid, Chip } from '@material-ui/core';

// Internal Components
import SectionTitle from './SectionTitle';
import SectionSkeleton from './SectionSkeleton';

// Icons
import { Web as WebIcon } from '@material-ui/icons';

function DomainsChips({ domains, loading, disabled }) {
    return loading ? (
        <SectionSkeleton />
    ) : (
        <>
            <SectionTitle title="Domains" icon={WebIcon} />
            <Grid container alignContent="center">
                {domains?.map(domain => (
                    <Chip
                        key={domain.name}
                        label={domain.name}
                        size="small"
                        disabled={disabled}
                        data-testid="domain-chip"
                    />
                ))}
            </Grid>
        </>
    );
}

export default DomainsChips;
