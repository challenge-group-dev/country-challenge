import React from 'react';
import { render } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';

import FlagCardDetails from '../FlagCardDetails';

describe('Input area testing', () => {
    let wrapper,
        props = null;

    props = {
        loading: false,
        onChange: jest.fn(),
        country: {
            name: 'Teste',
            capital: 'Teste',
            flag: 'teste',
        },
    };

    beforeEach(() => {
        wrapper = render(
            <MockedProvider>
                <FlagCardDetails {...props} />
            </MockedProvider>
        );
    });

    it('Should render correctly', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should show loading status', () => {
        const component = render(
            <MockedProvider>
                <FlagCardDetails loading />
            </MockedProvider>
        );

        expect(component).toBeDefined();
    });
});
