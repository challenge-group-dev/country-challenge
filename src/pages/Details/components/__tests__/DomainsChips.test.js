import React from 'react';
import { render } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';

import DomainsChips from '../DomainsChips';

describe('Domains Chips testing', () => {
    let wrapper,
        props = null;

    props = {
        loading: false,
        domains: [
            {
                name: 'fr',
            },
        ],
    };

    beforeEach(() => {
        wrapper = render(
            <MockedProvider>
                <DomainsChips {...props} />
            </MockedProvider>
        );
    });

    it('Should render correctly', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should show loading status', () => {
        const component = render(
            <MockedProvider>
                <DomainsChips loading />
            </MockedProvider>
        );

        expect(component).toBeDefined();
    });
});
