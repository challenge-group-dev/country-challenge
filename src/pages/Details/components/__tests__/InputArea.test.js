import React from 'react';
import { render, fireEvent, act } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';

import InputArea from '../InputArea';

describe('Input area testing', () => {
    let wrapper,
        props = null;

    props = {
        loading: false,
        onChange: jest.fn(),
    };

    beforeEach(() => {
        wrapper = render(
            <MockedProvider>
                <InputArea {...props} />
            </MockedProvider>
        );
    });

    it('Should render correctly', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should show loading status', () => {
        const component = render(
            <MockedProvider>
                <InputArea loading />
            </MockedProvider>
        );

        expect(component).toBeDefined();
    });

    it('Should call onSearch', () => {
        act(() => {
            fireEvent.change(wrapper.getByTestId('input-area'), {
                target: { value: 'test' },
            });
            expect(props.onChange).toHaveBeenCalled();
        });
    });
});
