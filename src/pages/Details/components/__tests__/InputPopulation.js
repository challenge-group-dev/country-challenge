import React from 'react';
import { render, act, fireEvent } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';

import InputPopulation from '../InputPopulation';

describe('Input population testing', () => {
    let wrapper,
        props = null;

    props = {
        loading: false,
        onChange: jest.fn(),
    };

    beforeEach(() => {
        wrapper = render(
            <MockedProvider>
                <InputPopulation {...props} />
            </MockedProvider>
        );
    });

    it('Should render correctly', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should show loading status', () => {
        const component = render(
            <MockedProvider>
                <InputPopulation loading />
            </MockedProvider>
        );

        expect(component).toBeDefined();
    });

    it('Should call onSearch', () => {
        act(() => {
            fireEvent.change(wrapper.getByTestId('input-population'), {
                target: { value: 'test' },
            });
            expect(props.onChange).toHaveBeenCalled();
        });
    });
});
