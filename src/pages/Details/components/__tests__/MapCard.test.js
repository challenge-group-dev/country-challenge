import React from 'react';
import { render } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';

import MapCard from '../MapCard';

import { GET_BORDER_COUNTRIES_QUERY } from 'services/country/useGetBorderCountries';

describe('MapCard testing', () => {
    let wrapper,
        props = null;

    props = {
        loading: false,
        center: {
            latitude: 60.116667,
            longitude: 19.9,
        },
    };

    const gqlMock = [
        {
            request: {
                query: GET_BORDER_COUNTRIES_QUERY,
                variables: {
                    filter: {
                        location_distance_lt: {
                            point: {
                                latitude: props.center.latitude,
                                longitude: props.center.longitude,
                            },
                            distance: 1500000,
                        },
                    },
                },
            },
            result: {
                data: {
                    Country: [
                        {
                            _id: '27',
                            name: 'Åland Islands',
                            location: {
                                latitude: 60.116667,
                                longitude: 19.9,
                                __typename: '_Neo4jPoint',
                            },
                            __typename: 'Country',
                        },
                        {
                            _id: '323',
                            name: 'Austria',
                            location: {
                                latitude: 47.33333333,
                                longitude: 13.33333333,
                                __typename: '_Neo4jPoint',
                            },
                            __typename: 'Country',
                        },
                    ],
                },
            },
        },
    ];

    beforeEach(() => {
        wrapper = render(
            <MockedProvider mocks={gqlMock}>
                <MapCard {...props} />
            </MockedProvider>
        );
    });

    it('Should render correctly', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should show loading status', () => {
        const component = render(
            <MockedProvider mocks={gqlMock}>
                <MapCard center={props.center} />
            </MockedProvider>
        );

        expect(component).toBeDefined();
    });
});
