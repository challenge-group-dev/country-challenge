import React from 'react';

// Components
import { Card, Tooltip } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import { Room as RoomIcon } from '@material-ui/icons';

// Styles
import { makeStyles } from '@material-ui/core/styles';

// Map
import ReactMapGL, { Marker } from 'react-map-gl';

// Services
import useGetBorderCountries from 'services/country/useGetBorderCountries';

const useStyles = makeStyles(theme => ({
    markIcon: {
        fontSize: '35px',
        transition: 'all 0.2s',
        zIndex: '8888',
        '&:hover': {
            color: '#3f51b5',
            zIndex: '9999',
            textShadow: '0 0 5px 5px black',
            position: 'absolute',
            top: '-100%',
        },
    },
}));

function MapCard({ center }) {
    const classes = useStyles();

    const { countries, loading } = useGetBorderCountries({
        filter: {
            location_distance_lt: {
                point: {
                    latitude: center.latitude,
                    longitude: center.longitude,
                },
                distance: 1500000,
            },
        },
    });

    const Markers = countries?.map(
        (item, index) =>
            index < 6 && (
                <Marker
                    key={item._id}
                    latitude={item?.location?.latitude}
                    longitude={item?.location?.longitude}
                    offsetLeft={-20}
                    offsetTop={-10}
                >
                    <Tooltip title={item?.name}>
                        <RoomIcon className={classes.markIcon} />
                    </Tooltip>
                </Marker>
            )
    );

    return loading || !center?.latitude || !center?.longitude ? (
        <Skeleton animation="wave" width="100%" height={500} />
    ) : (
        <Card variant="outlined" className="map">
            <ReactMapGL
                latitude={center?.latitude}
                longitude={center?.longitude}
                width="100%"
                height={500}
                mapboxApiAccessToken="pk.eyJ1IjoiaXRhbG9tYWlvIiwiYSI6ImNrZDZkbWtwdjBoYTEycnF2enYyYTI2MGwifQ.3Jza44kh5dVoMf2qCPDjpg"
                mapStyle="mapbox://styles/mapbox/streets-v8"
                zoom={2}
            >
                {Markers}
            </ReactMapGL>
        </Card>
    );
}

export default MapCard;
