import React from 'react';

// Components
import { FormControl, OutlinedInput, InputAdornment } from '@material-ui/core';

// Internal Components
import SectionTitle from './SectionTitle';
import SectionSkeleton from './SectionSkeleton';

// Icons
import { TrackChanges as TrackChangesIcon } from '@material-ui/icons';

// Styles
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    adornment: {
        position: 'relative',
        fontSize: '0.8rem',
        right: '-5px',
    },
});

function InputArea({ onChange, value, disabled, loading }) {
    const classes = useStyles();

    return loading ? (
        <SectionSkeleton />
    ) : (
        <>
            <SectionTitle title="Area" icon={TrackChangesIcon} />
            <FormControl size="small" variant="outlined">
                <OutlinedInput
                    size="small"
                    id="outlined-adornment-weight"
                    value={value}
                    onChange={onChange}
                    disabled={disabled}
                    endAdornment={
                        <InputAdornment position="end">
                            <span className={classes.adornment}>
                                Km<sup>2</sup>
                            </span>
                        </InputAdornment>
                    }
                    aria-describedby="outlined-weight-helper-text"
                    inputProps={{
                        'aria-label': 'weight',
                        'data-testid': 'input-area',
                    }}
                    labelWidth={0}
                />
            </FormControl>
        </>
    );
}

export default InputArea;
