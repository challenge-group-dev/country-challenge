import React from 'react';

// Components
import { FormControl, OutlinedInput } from '@material-ui/core';

// Internal Components
import SectionTitle from './SectionTitle';
import SectionSkeleton from './SectionSkeleton';

// Icons
import { People as PeopleIcon } from '@material-ui/icons';

function InputPopulation({ onChange, value, disabled, loading }) {
    return loading ? (
        <SectionSkeleton />
    ) : (
        <>
            <SectionTitle title="Population" icon={PeopleIcon} />
            <FormControl size="small" variant="outlined">
                <OutlinedInput
                    size="small"
                    id="outlined-adornment-weight"
                    value={value}
                    onChange={onChange}
                    disabled={disabled}
                    aria-describedby="outlined-weight-helper-text"
                    inputProps={{
                        'aria-label': 'weight',
                        'data-testid': 'input-population',
                    }}
                    labelWidth={0}
                />
            </FormControl>
        </>
    );
}

export default InputPopulation;
