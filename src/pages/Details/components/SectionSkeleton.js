import React from 'react';

// Components
import { Skeleton } from '@material-ui/lab';

function SectionSkeleton() {
    return (
        <>
            <Skeleton animation="wave" height={30} />
            <Skeleton animation="wave" height={50} />
        </>
    );
}

export default SectionSkeleton;
