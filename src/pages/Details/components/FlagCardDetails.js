import React from 'react';

// Components
import { Card, CardContent, Typography, CardMedia } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';

// Styles
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    card: {
        display: 'flex',
    },
    details: {
        display: 'flex',
        flex: '1 0 auto',
        flexDirection: 'column',
        alignSelf: 'center',
    },
    cover: {
        width: 200,
        height: 150,
    },
});

function FlagCardDetails({ loading, country }) {
    const classes = useStyles();

    return loading ? (
        <Skeleton animation="wave" component="div" height={200} />
    ) : (
        <Card variant="outlined" className={classes.card}>
            <div className={classes.details}>
                <CardContent className={classes.content}>
                    <Typography component="h5" variant="h5">
                        {country.name}
                    </Typography>
                    <Typography variant="subtitle1" color="textSecondary">
                        {country.capital}
                    </Typography>
                </CardContent>
            </div>
            <CardMedia
                className={classes.cover}
                image={country?.flag?.svgFile}
                title="Live from space album cover"
                component="image"
            />
        </Card>
    );
}

export default FlagCardDetails;
