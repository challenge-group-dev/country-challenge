// Apollo
import { useMutation, gql } from '@apollo/client';

const MUTATION_UPDATE_SEARCH = gql`
    mutation updateSearchKeyword($input: String!) {
        updateSearchKeyword(input: $input) @client
    }
`;

function useSetSearchKeyword() {
    const [onSearchKeyword, { data, error, loading }] = useMutation(
        MUTATION_UPDATE_SEARCH
    );

    return { onSearchKeyword, data, loading, error };
}

export default useSetSearchKeyword;
