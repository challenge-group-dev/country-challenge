import { useMemo } from 'react';
import { gql, useQuery } from '@apollo/client';

export const GET_COUNTRIES_QUERY = gql`
    query($filter: _CountryFilter!, $first: Int!, $offset: Int!) {
        Country(filter: $filter, first: $first, offset: $offset) {
            _id
            name
            capital
            flag {
                svgFile
            }
        }
    }
`;

function useGetCountries(variables = {}, options = {}) {
    const { error, data, loading, refetch, fetchMore } = useQuery(
        GET_COUNTRIES_QUERY,
        {
            variables: {
                filter: {
                    ...variables?.filter,
                },
                ...variables,
            },
            ...options,
            notifyOnNetworkStatusChange: true,
        }
    );

    const countries = useMemo(() => data?.Country || [], [data]);

    return { countries, loading, error, refetch, fetchMore };
}

export default useGetCountries;
