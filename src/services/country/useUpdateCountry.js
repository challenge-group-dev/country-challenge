import { useMutation, gql } from '@apollo/client';

const MUTATION_UPDATE_COUNTRY = gql`
    mutation updateCountry($id: String!, $data: Object!) {
        updateCountry(id: $id, data: $data) @client
    }
`;

function useUpdateCountry(variables = {}, options = {}) {
    const [onUpdateCountry, { error, loading }] = useMutation(
        MUTATION_UPDATE_COUNTRY
    );

    return { onUpdateCountry, error, loading };
}

export default useUpdateCountry;
