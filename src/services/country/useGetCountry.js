import { useMemo } from 'react';
import { gql, useQuery } from '@apollo/client';

export const GET_COUNTRY_QUERY = gql`
    query($id: String) {
        Country(_id: $id) {
            _id
            name
            capital
            area
            population
            topLevelDomains {
                name
                __typename
            }
            flag {
                svgFile
                __typename
            }
            location {
                latitude
                longitude
                __typename
            }
            __typename
        }
    }
`;

function useGetCountry(variables = {}, options = {}) {
    const { error, data, loading, refetch } = useQuery(GET_COUNTRY_QUERY, {
        variables: {
            filter: {
                ...variables?.filter,
            },
            ...variables,
        },
        ...options,
    });

    const country = useMemo(() => data?.Country[0] || {}, [data]);

    return { country, loading, error, refetch };
}

export default useGetCountry;
