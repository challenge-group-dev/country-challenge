import { useMemo } from 'react';
import { gql, useQuery } from '@apollo/client';

export const GET_BORDER_COUNTRIES_QUERY = gql`
    query($filter: _CountryFilter!) {
        Country(filter: $filter) {
            _id
            name
            location {
                latitude
                longitude
            }
        }
    }
`;

function useGetCountries(variables = {}, options = {}) {
    const { error, data, loading, refetch } = useQuery(
        GET_BORDER_COUNTRIES_QUERY,
        {
            variables: {
                filter: {
                    ...variables?.filter,
                },
                ...variables,
            },
            ...options,
        }
    );

    const countries = useMemo(() => data?.Country || [], [data]);

    return { countries, loading, error, refetch };
}

export default useGetCountries;
