import React from 'react';
import { render, fireEvent, act, wait, screen } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';

import { GET_COUNTRIES_QUERY } from 'services/country/useGetCountries';
import { GET_COUNTRY_QUERY } from 'services/country/useGetCountry';

import App from './App';

describe('Render testing', () => {
    const gqlMock = [
        {
            request: {
                query: GET_COUNTRIES_QUERY,
                variables: {
                    filter: {},
                    first: 20,
                    offset: 0,
                },
            },
            result: {
                data: {
                    Country: [
                        {
                            _id: '1',
                            name: 'Marcelino',
                            capital: 'teste',
                            flag: { svgFile: 'teste', __typename: 'Flag' },
                            __typename: 'Country',
                        },
                        {
                            _id: '2',
                            name: 'Marcelino',
                            capital: 'teste',
                            flag: { svgFile: 'teste', __typename: 'Flag' },
                            __typename: 'Country',
                        },
                    ],
                },
            },
        },
        {
            request: {
                query: GET_COUNTRIES_QUERY,
                variables: {
                    filter: { name_contains: '' },
                    first: 20,
                    offset: 0,
                },
            },
            result: {
                data: {
                    Country: [
                        {
                            _id: '1',
                            name: 'Marcelino',
                            capital: 'teste',
                            flag: { svgFile: 'teste', __typename: 'Flag' },
                            __typename: 'Country',
                        },
                        {
                            _id: '2',
                            name: 'Marcelino',
                            capital: 'teste',
                            flag: { svgFile: 'teste', __typename: 'Flag' },
                            __typename: 'Country',
                        },
                    ],
                },
            },
        },
    ];

    let wrapper = null;
    beforeAll(() => {
        wrapper = render(
            <MockedProvider addTypename={false} mocks={gqlMock}>
                <App />
            </MockedProvider>
        );
    });

    it('should render correctly', () => {
        expect(wrapper).toBeDefined();
    });
});
